
import error from './errorImage.jpg'
import React, { Component } from 'react'
import './MainComponent.css'

export class ErrorOccurred extends Component {
  render() {
    return (
      <div>
        <h2>Sorry, couldn't fetch data from our server, please try again later</h2>
        <img src={error} alt='error' className='error'/>
      </div>
    )
  }
}

export default ErrorOccurred