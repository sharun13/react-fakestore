import React from 'react'

export default function Items(props) {
    return (
        <div>
            <div className="product-card-container">
                <h1 className="product-title">{props.title}</h1>
                <img src={props.image} alt="product" className="product-image" />
                <p className="category">{props.category}</p>
                <button className="rating">{props.rating} &#9733; <span>{props.count} Ratings</span></button>
                <p className="price"> &#x20B9; {props.price}</p>
                <p className="description">{props.description}</p>
            </div>
        </div>
    )
}


