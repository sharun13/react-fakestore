import React, { Component } from 'react'
import '../index.css'
import logo from './logo'

class Header extends Component {
  render() {
    return (
      <div>
        <header className='header'>
            <img src = {logo} alt="logo" className='header-logo'/>
            <h1>E-Commerce Store</h1>
        </header>
      </div>
    )
  }
}

export default Header