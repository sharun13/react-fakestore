import React, { Component } from 'react'
import './Loader.css'

export class Loader extends Component {
    render() {
        return (
            <div>
                <div className="lds-roller">
                    <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
                </div>
            </div>
        )
    }
}

export default Loader