import React, { Component } from 'react'
import axios from 'axios'
import './MainComponent.css'
import Loader from './Loader'
import ErrorOccurred from './ErrorOccurred'
import Items from './Items'

class MainComponent extends Component {

    APP_STATE = {
        LOADING: 'loading',
        LOADED: 'loaded',
        ERROR: 'error',
    }

    constructor(props) {
        super(props)

        this.state = {
            apiResult: [],
            status: this.APP_STATE.LOADING,
        }
    }

    getData = () => {
        axios.get('https://fakestoreapi.com/products')
            .then((response) => {
                this.setState({
                    apiResult: response.data,
                    status: this.APP_STATE.LOADED,
                })
            }
            ).catch((error) => {
                this.setState({
                    status: this.APP_STATE.ERROR
                })
                console.log(error)
            })

    }

    componentDidMount() {
        this.getData()
    }

    render() {

        return (
            <div>
                <h1 className="dash-heading">Explore The Latest Fashion</h1>
                <div className="main-container">
                    {this.state.status === this.APP_STATE.LOADING ?
                        <>
                            <Loader />
                        </>
                        :
                        undefined
                    }
                    {this.state.status === this.APP_STATE.ERROR ?
                        <>
                            <ErrorOccurred></ErrorOccurred>
                        </>
                        :
                        undefined
                    }
                    {this.state.status === this.APP_STATE.LOADED ?
                        <>
                            {this.state.apiResult.length === 0 ?
                                <h2>
                                    Sorry No Products To Display Now
                                </h2>
                                :
                                this.state.apiResult.map((item) => {
                                    return (
                                        <Items
                                            key={item.id}
                                            title={item.title}
                                            image={item.image}
                                            category={item.category}
                                            rating={item.rating.rate}
                                            count={item.rating.count}
                                            price={`${(item.price * 80).toFixed(2)}`}
                                            description={item.description}
                                        >
                                        </Items>
                                    )
                                })
                            }
                        </>
                        :
                        undefined
                    }
                </div>
            </div>
        )
    }
}

export default MainComponent