import './App.css';
import Header from './components/Header';
import MainComponent from './components/MainComponent';

function App() {
  return (
    <div className="App">
      <Header></Header>
      <MainComponent></MainComponent>
    </div>
  );
}

export default App;
